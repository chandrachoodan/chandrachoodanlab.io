---
title: Nitin Chandrachoodan
author: Nitin Chandrachoodan
---

![](nitin.jpg){ .biopic width=30% }
I am a professor in the department of [Electrical Engineering](http://www.ee.iitm.ac.in/) at [IIT Madras](https://www.iitm.ac.in/). My undergraduate degree was in Electronics and Communication Engineering from the same department in 1996, followed by a PhD from the [University of Maryland at College Park](https://www.ece.umd.edu/) in 2002.

I primarily work in the areas of digital system design and VLSI / FPGA implementations of DSP systems, and am part of the [Integrated Circuits and Systems Group](http://www.ee.iitm.ac.in/ics/) at IIT Madras.

### Contact information

* **Office:**   ESB 245C
* **Email:**   nitin@ee.iitm.ac.in
* **Phone:**  +91-44-2257 4432
* **Web:**   <https://chandrachoodan.gitlab.io>

