---
title: Miscellany
---

Various pieces of information have floated in and out of this page over the years.  The entire site itself has undergone many revisions, mostly in form but not so much in content.  After several years of this, I have come to the conclusion that maintaining a useful and relevant web page is not a realistic goal, and the entire notion of a web page needs to be reexamined.

Anyway, since I am too lazy to actually think through this line of thought, I have tried to simplify the page design down to bare [markdown](https://daringfireball.net/projects/markdown/), [Pandoc](https://pandoc.org), a [makefile](https://github.com/fcanas/bake), and a nice looking [CSS template](https://writ.cmcenroe.me).  Thanks to the authors of each of these and other open source systems for sharing.
