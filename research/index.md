---
title: Research
---

I work primarily in the areas of digital system design, mostly targeting FPGA based designs.  Some of the broad areas are:

* Approximate computing
* Low-power signal processing
* Architectures for machine learning and techniques for low power/low complexity computation

My profile on [Google Scholar](https://scholar.google.co.in/citations?user=_S6HjK0AAAAJ&hl=en)
