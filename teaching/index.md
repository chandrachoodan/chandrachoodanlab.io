---
title: Teaching
---

I will be teaching [EE2003 (Computer Organization)](/teaching/ee2003/) in the Aug 2021 semester.  

> Note: I will try my best to accommodate as many students as possible for this course as it is running in online mode.  However, students should be aware that the expectations from assignments and evaluation mechanisms are likely to change to adapt to conditions.

Some courses I taught previously include:

* EE 2003 - Computer Organization.  [Videos from Aug 2020](https://www.youtube.com/playlist?list=PLco7dux9L7g2GM-rwKf0DPrHItgsVWxiH) are on YouTube
* EE 5332 - Mapping DSP Algorithms to Architectures (Jan 2019) - unedited videos of the lectures are available at [YouTube](https://www.youtube.com/playlist?list=PLco7dux9L7g1RrB8TqUVCMEeu86D7azeg)
* EE 5311 - Digital IC Design (multiple times)
* EE 4371 - Data Structures and Algorithms (Aug 2018)
* EE 5703 - FPGA lab. There was previously material linked here but it is out of date.