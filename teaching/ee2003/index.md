---
title: EE 2003 Computer Organization
---

> Note: This page is subject to change.  The broad outline of the course will remain the same, but details will change.  In particular the grading mechanisms and assignments are likely to change.

## Abstract
The purpose of this course is to answer a few basic questions:

* What actually happens inside a computer when you compile and run a program?
* How does a processor perform different kinds of tasks?
* What makes a computer "good"? How do you evaluate this, and how can it be improved?

We start with the core idea of a finite state machine that can be "programmed", see how to define metrics to evaluate the performance of such a system, and how better systems can be built with this knowledge.  An important aspect here is that the performance of a computer system is determined by much more than just the processor core, and understanding the interaction of the parts of the system is important to getting the most out of it.

This course is tightly rooted in engineering practice, and as such requires significant hands-on programming to get the most out of it.  There will be programming assignments in Verilog, and some knowledge of C and assembly will also be required.  Knowledge of simple Python programming will also be helpful.

The [RISC-V](https://riscv.org/) instruction set will be used as the basis for implementation.  The minimum goal of the course is to build a Verilog implementation capable of executing the base RV32I instruction set.

## Target audience
The course is positioned as a 3rd year (5th semester) course, as a follow-up to digital logic and microprocessor theory+lab.  This course will go into more depth on both software and hardware issues, but will not cover advanced topics in processor architecture (out-of-order, superscalar etc.) in any depth.  It should be suitable as a precursor to a more advanced computer architecture course.

## Pre-requisites
* Digital logic: combinational and sequential circuits
* Working knowledge of C programming, especially pointers
* Basic knowledge of assembly programming (for any processor)
* Verilog HDL: implementing basic combinational and sequential systems

## Topic outline 
***(subject to change)***

* First steps: what happens when you compile and run a program (1 wk)
* Review of digital logic design (1 wk)
* Instructions, translating/assembling, compiling/optimizing, running a program (2 wk)
* Arithmetic: integers, floating point (1 wk)
* Evaluation: Benchmarks and performance evaluation (1 wk)
* Processor: datapath and control, multicycle implementation (2 wk)
* Pipelining and performance enhancement (2 wk)
* Memory hierarchy: cache, virtual memory (2 wk)
* Buses: interfacing peripherals, memory mapping (1 wk)
* MPSoC (1 wk)

## Mode of Operation

Due to Covid-19, we do not expect in-person classes to happen this semester, and this course will run entirely online.  However, it will still be largely lab based - the main difference is that you cannot actually see your designs running on hardware.

### Classes

Material for each week will be made available online: it will be a combination of online videos, reading material, and exercises.  There will be meetings in the scheduled slot, but in smaller groups, not the entire class.  You are expected to follow the material on your own, and use the meetings to get your doubts cleared and problems resolved.

Assignments will be handled either through Moodle or through other online submission methods.

***IMPORTANT*** An essential pre-requisite for this course is that you have a *working* internet connection.  It does not have to be fast, but you should be in a position to access the material and solve the assignments online.  In most cases, you can do this on your own systems and submit the final assignments, so even a moderate quality of connection should be enough.  

### Communication

Course material will be communicated mostly through Moodle, although I am trying to find other effective and fast means of communication.  Suggestions are welcome.

## Evaluation

This course is intended as a practical course, and in Aug 2019 was extensively done in the lab.  Due to the prevailing circumstances in Aug-Dec 2020, direct board level implementation will not be possible.  However, the assignments will still involve Verilog and C programming, and will constitute the major part of the grade (~60%).  In addition, there will be online quizzes in Moodle (~10%) and one or two written exams (~30%).  The exact breakup is subject to change.

### A note on S grades

Due to the largely online nature of the course, it is difficult to judge the quality of individual work.  For the most part, you are assumed to be here because you are actually interested in learning the course material, and you will be expected to follow an honour code, which basically states that you will do your own work, and will acknowledge clearly any assistance you get from others.

For the top grade (S), you will be expected to do something clearly above and beyond the basic requirements of the class.  I cannot say in advance what this is, but you are free to discuss options as we go through the course.  For now, all I can say is that maxing out all assignments, quizzes and written exams can get you up to an A, but not an S.

An example of the extra work would be implementing the additional instructions required to get Linux booting on the system, and actually demonstrating Linux running on your design.  However, this has to be your own system - there are freely available codes for RISC-V on the net, and using one of those for a demo is worse than useless.  The bottom-line here is that the S grade will require seriously convincing me of why your work has gone above and beyond the basic requirements of the course.

## Reference material

The main text for the course is "Computer Organization and Design: The Hardware/Software Interface - RISC-V edition", D. A. Patterson and J. L. Hennessy, Morgan-Kaufmann publishers.  This book is not easily available in India at the moment, so I will be supplementing this with material posted on Moodle that is relevant to specific chapters being discussed.

There is a lot of online material already available that will be used for reference.

### On assignments

Note that there already exist several Verilog implementations of the RISC-V processor, including a signficant amount of code in the reference book itself.  As before, you are expected to not copy this code blindly, though I cannot prevent you from referring to it.  In such cases, you need to be clear about what parts were referenced and what you have added to your assignment above what was already there.

